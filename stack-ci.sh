#! /usr/bin/env nix-shell
#! nix-shell -p git stack zlib glibcLocales haskell.compiler.ghc865 -i bash -I nixpkgs="https://github.com/NixOS/nixpkgs/archive/a4d05ec30c6636f8f1268efb8ddf6f9f8b7ca507.tar.gz" --fallback
rm -rf $PWD/fake-home
mkdir -p $PWD/fake-home/.stack
export HOME=$PWD/fake-home
export LANG=en_US.utf8
export LC_ALL=C.UTF-8
cat <<EOF > $HOME/.stack/config.yaml
  nix:
    packages:
    - zlib
    - zlib.dev
    - zlib.out
    enable: true
    add-gc-roots: true
  system-ghc: true
EOF
git config --global http.sslVerify false
stack build
